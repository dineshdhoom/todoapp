 
const mongoose = require('mongoose')

const Todo = mongoose.model('Todo', {
 _id: string,
 id: number,
 title: String,
 complete: boolean,
})

module.exports = {Todo} 