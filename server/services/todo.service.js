var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('todos');

var service = {};

service.getAll = getAll;
service.getById = getById;
service.create = create;
service.update = update;
service.delete = _delete;

module.exports = service;


function getAll() {
    var deferred = Q.defer();

    db.todos.find().toArray(function (err, todos) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        deferred.resolve(todos);
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.todos.findById(_id, function (err, todo) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (todo) {
            // return user (without hashed password)
            deferred.resolve(_.omit(todo, 'hash'));
        } else {
            // user not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function create(todoParam) {
    var deferred = Q.defer();

    db.todos.insert(
        todoParam,
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });
    

    return deferred.promise;
}

function update(_id, todoParam) {
    var deferred = Q.defer();

    // fields to update
    var set = {
        title: todoParam.title,
        complete: todoParam.complete,
    };

    db.todos.update(
        { _id: mongo.helper.toObjectID(_id) },
        { $set: set },
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });
    
    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.todos.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}