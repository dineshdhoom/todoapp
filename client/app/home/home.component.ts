﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Todo } from '../_models/index';
import { AlertService, TodoService } from '../_services/index';


@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
    todos: Todo[] = [];
    newTodo: Todo = new Todo();
    
    constructor(
        private router: Router,
        private todoService: TodoService,
        private alertService: AlertService) { }

    ngOnInit() {
        this.loadAllTodos();
    }

    deleteTodo(_id: string) {
        this.todoService.delete(_id).subscribe(() => { this.loadAllTodos() });
        return this;
    }

    addTodo() {
        this.todoService.create(this.newTodo).subscribe(
            data => {
                this.alertService.success('Todo Added', true);
                this.todos.push(this.newTodo);
                this.newTodo = new Todo();
                this.router.navigate(['/']);
            },
            error => {
                this.alertService.error(error);
            });
        return this;
    }
    
    toggleTodoComplete(todo: Todo) {
        this.todoService.toggleTodoComplete(todo).subscribe(
            data => {
                this.alertService.success('Todo Updated', true);
                this.router.navigate(['/']);
            },
            error => {
                this.alertService.error(error);
            });
        return todo;
    }
    
    removeTodo(todo: Todo) {
        this.todoService.delete(todo._id);
        return this;
    }

    private loadAllTodos() {
        this.todoService.getAll().subscribe(todos => { this.todos = todos; });
    }
}