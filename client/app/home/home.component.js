"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var index_1 = require("../_models/index");
var index_2 = require("../_services/index");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(router, todoService, alertService) {
        this.router = router;
        this.todoService = todoService;
        this.alertService = alertService;
        this.todos = [];
        this.newTodo = new index_1.Todo();
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.loadAllTodos();
    };
    HomeComponent.prototype.deleteTodo = function (_id) {
        var _this = this;
        this.todoService.delete(_id).subscribe(function () { _this.loadAllTodos(); });
        return this;
    };
    HomeComponent.prototype.addTodo = function () {
        var _this = this;
        this.todoService.create(this.newTodo).subscribe(function (data) {
            _this.alertService.success('Todo Added', true);
            _this.todos.push(_this.newTodo);
            _this.newTodo = new index_1.Todo();
            _this.router.navigate(['/']);
        }, function (error) {
            _this.alertService.error(error);
        });
        return this;
    };
    HomeComponent.prototype.toggleTodoComplete = function (todo) {
        var _this = this;
        this.todoService.toggleTodoComplete(todo).subscribe(function (data) {
            _this.alertService.success('Todo Updated', true);
            _this.router.navigate(['/']);
        }, function (error) {
            _this.alertService.error(error);
        });
        return todo;
    };
    HomeComponent.prototype.removeTodo = function (todo) {
        this.todoService.delete(todo._id);
        return this;
    };
    HomeComponent.prototype.loadAllTodos = function () {
        var _this = this;
        this.todoService.getAll().subscribe(function (todos) { _this.todos = todos; });
    };
    HomeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: 'home.component.html'
        }),
        __metadata("design:paramtypes", [router_1.Router,
            index_2.TodoService,
            index_2.AlertService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map