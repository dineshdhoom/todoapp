"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var TodoService = /** @class */ (function () {
    function TodoService(http) {
        this.http = http;
    }
    TodoService.prototype.getAll = function () {
        return this.http.get('/todos').map(function (response) { return response.json(); });
    };
    TodoService.prototype.getById = function (_id) {
        return this.http.get('/todos/' + _id).map(function (response) { return response.json(); });
    };
    TodoService.prototype.create = function (todo) {
        return this.http.post('/todos', todo);
    };
    TodoService.prototype.toggleTodoComplete = function (todo) {
        return this.http.put('/todos/' + todo._id, { title: todo.title, complete: !todo.complete });
    };
    TodoService.prototype.update = function (todo) {
        return this.http.put('/todos/' + todo._id, todo);
    };
    TodoService.prototype.delete = function (_id) {
        return this.http.delete('/todos/' + _id);
    };
    TodoService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], TodoService);
    return TodoService;
}());
exports.TodoService = TodoService;
//# sourceMappingURL=todo.service.js.map