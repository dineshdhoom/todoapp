import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Todo } from '../_models/index';

@Injectable()
export class TodoService {
    constructor(private http: Http) { }

    getAll() {
        return this.http.get('/todos').map((response: Response) => response.json());
    }

    getById(_id: string) {
        return this.http.get('/todos/' + _id).map((response: Response) => response.json());
    }

    create(todo: Todo) {
        return this.http.post('/todos', todo);
    }

    toggleTodoComplete(todo: Todo) {
        return this.http.put('/todos/' + todo._id, {title: todo.title, complete: !todo.complete}); 
    }

    update(todo: Todo) {
        return this.http.put('/todos/' + todo._id, todo);
    }

    delete(_id: string) {
        return this.http.delete('/todos/' + _id);
    }
}