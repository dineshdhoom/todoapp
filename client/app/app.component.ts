﻿import { Component } from '@angular/core';
import { AuthenticationService } from './_services/index';

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html'
})

export class AppComponent { 
	isAuhtenticated: boolean;

	constructor(private authService: AuthenticationService) { }

	isUserLoggedIn() {
        this.isAuhtenticated = this.authService.isUserLoggedIn();
        return this.isAuhtenticated;
    }
}