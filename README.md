# TodoApp

Steps to run this app

## Build and run client

1 - Go inside client folder and run npm install

2 - npm start

## Build and run server

1 - Go inside server folder

2 - node server.js


## Future improvements

1 - currently todo list has to be reloaded to referesh the data from server

2 - Remove todo support needs to be added


